from os import environ
from csv import DictReader

import click
from dotenv import load_dotenv
load_dotenv()

GEONAME_ID_FILE = './GeoLite2-Country-Locations-en.csv'
IPV4_SUBNET_FILE = './GeoLite2-Country-Blocks-IPv4.csv'

def init_geonames(geoname_file, include):
    geonames = {}
    with open(geoname_file) as fp:
        geoname_reader = DictReader(fp)
        for row in geoname_reader:
            country_iso_code = row['country_iso_code']
            if country_iso_code not in include:
                continue
            geonames[row['geoname_id']] = row
    return geonames


def init_ipv4(subnet_file, include):
    ipv4_blocks = []
    with open(subnet_file) as fp:
        ipv4_reader = DictReader(fp)
        for row in ipv4_reader:
            geoname_id = row['geoname_id']
            if geoname_id not in include:
                continue
            ipv4_blocks.append(row)
    return ipv4_blocks


@click.command()
@click.option(
    '--include', '-i',
    multiple=True,
    help='Which country iso codes to include (example: SE)'
)
@click.option(
    '--exclude', '-x',
    multiple=True,
    help='Not implemented'
)
@click.option(
    '--geoname-csv',
    default=environ.get(
        'GEONAME_ID_FILE',
        GEONAME_ID_FILE
    ),
    type=click.Path()
)
@click.option(
    '--subnet-csv',
    default=environ.get(
        'IPV4_SUBNET_FILE',
        IPV4_SUBNET_FILE
    ),
    type=click.Path()
)
def lookup_country(include, exclude, geoname_csv, subnet_csv):
    geonames = init_geonames(geoname_csv, include)
    ipv4_blocks = init_ipv4(subnet_csv, geonames)

    for block in ipv4_blocks:
        print(block['network'])


if __name__ == '__main__':
    exit(lookup_country(None))
