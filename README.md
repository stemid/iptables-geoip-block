# IPtables Geo IP block

This is a different take on blocking countries in your IPtables firewall.

A lot of guides online recommend you use an xtables module for this, which requires compilation. I loathe all things not managed by packages because of poor lifecycle management so I decided to take a different approach.

# Setup

## Download Maxmind GeoLite Country database

    $ curl -o GeoIPCountryCSV.zip https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country-CSV.zip
    $ unzip GeoIPCountryCSV.zip
    GeoLite2-Country-CSV_20191029/
    GeoLite2-Country-CSV_20191029/GeoLite2-Country-Blocks-IPv6.csv
    GeoLite2-Country-CSV_20191029/GeoLite2-Country-Locations-en.csv
    GeoLite2-Country-CSV_20191029/GeoLite2-Country-Blocks-IPv4.csv
    ...

## Setup Python3 environment

    $ python3 -m venv .venv
    $ ./.venv/bin/pip install -r requirements.txt

# Run

    $ ./.venv/bin/python lookup_country.py --help
    $ ./.venv/bin/python lookup_country.py -i SE -i FI

This would list all IPv4-subnets for Sweden and Finland.

# IPtables

## Install ipset

Most systems require you to install ipset, here's a CentOS example.

    $ sudo yum install ipset ipset-service
    $ sudo systemctl enable ipset
    $ sudo systemctl start ipset

The service is only a method of loading ipset lists at boot, on Debian this is called ``ipset-persistent``.

## Configure ipset

Define all the ipset lists you want to create with the ipset command.

    ipset create geoip-SE hash:net hashsize 65535 counters
    ipset create geoip-FI hash:net hashsize 65535 counters

**Warning**: Some countries may have more than 65535 IP-subnets, like the United States.

    $ ./.venv/bin/python lookup_country.py -i US | wc -l

## Fill ipset

This needs to be done for each country's ipset list. Preferably in an automated way like a scheduled job.

Here's an example.

    $ while read -r subnet; do sudo ipset add geoip-SE "$subnet"; done < <(./.venv/bin/python lookup_country.py -i SE)

Verify by checking the ipset list command.

    $ ipset list geoip-SE

## Configure IPtables

To use an ipset in IPtables here is an example.

    iptables -A INPUT -m set --match-set geoip-SE src -j DROP

Dropping all traffic where src IP matches any in the geoip-SE ipset list.

Or, dropping all traffic that does **not** match the ipset.

    iptables -A INPUT -m set --set \!geoip-SE src -j DROP

# Automation Install docs

## Download CSV files

    #!/usr/bin/env bash

    geoip_dir=/var/opt/geoip
    mkdir -p "$geoip_dir"
    pushd "$geoip_dir"

    curl -s https://geolite.maxmind.com/download/geoip/database/GeoLite2-Country-CSV.zip -o GeoLite2CountryCSV.zip
    unzip GeoLite2CountryCSV.zip

    # Zip archive contains a directory with a date, avoid that by copying
    # the files out of it.
    find GeoLite2-Country-CSV* -name "*.csv" -type f | xargs -I{} cp {} ./

Now all the csv files are under /var/opt/geoip.

## Setup ipset

### CentOS

    $ sudo yum install ipset ipset-service

Setup ipset-service to restore ipsets from a file by first editing ``/etc/systemd/system/ipset.service.d/override.conf`` with this content.

    [Service]
    ExecStart=/sbin/ipset -exist -file /etc/ipset.conf restore
    ExecStop=/sbin/ipset destroy

Lastly edit ``/etc/ipset.conf`` with all your defined ipsets.

    create geoip-SE hash:net hashsize 65535 counters
    create geoip-FI hash:net hashsize 65535 counters

Same as the ipset commands earlier, just ommit the ipset part and list all your ipsets.

    $ sudo systemctl daemon-reload
    $ sudo systemctl enable ipset
    $ sudo systemctl restart ipset

### OpenBSD pf

In PF ipsets are called tables and created like this in the pf.conf rules.

    table <geoip-SE> {}

Or from a file of subnets like this.

    table <geoip-SE> persist file /etc/geoip-SE
    block in on $OutsideIF from <geoip-SE> to any

Or you can create them dynamically from the CLI while inserting subnets.

    $ sudo pfctl -t geoip-SE -T add 217.212.248.128/25
    1 table created.       
    1/1 addresses added.

## Autofill ipsets

First setup a virtualenv in the geoip dir created earlier.

    $ cd /var/opt/geoip
    $ python3 -m venv .venv
    $ ./.venv/bin/pip install -r requirements.txt

Then install this script in a scheduled job.

    #!/usr/bin/env bash

    geoip_dir=/var/opt/geoip
    ipsets='SE FI DK NO'

    pushd "$geoip_dir"
    source .venv/bin/activate

    for country in ipsets; do
      while read -r subnet; do
        ipset add "geoip-$country" "$subnet"
      done < <(python lookup_country.py -i "$country")
    done

## IPtables

Now use the new ipsets in IPtables however you want.

# Notes

1. xtables-addons is actually available as a package on Fedora 30.
